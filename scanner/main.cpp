#include <iostream>
#include "defs.h"
#define extern
#include "data.h"
#define extern
#include "decl.h"
#include <errno.h>

using namespace std;

static void init() {
    Line = 1;
    Putback = '\n';
}

static void usage(char *prog) {
    cout<<stderr<<"Usage: infile\n"<<prog;
    exit(1);
}

char *tokstr[] = {"+","-","*","/","int"};

static void scanfile() {
    struct token T;

    while (scan(&T))
    {
        cout<<"Token %s" <<tokstr[T.token];
      if (T.token == T_INTLIT)
         cout<<", value %d"<<T.intvalue<<endl;
    }   
}

void main (int argc, char *argv[]) {
    if(argc != 2)
    usage(argv[0]);

    init();

    if ((Infile = fopen(argv[1], "r")) == NULL) {
        cout<<stderr<<"Unable to open %s: %s\n"<<argv[1]<<strerror(errno);
        exit(1);
    }
    scanfile();
    exit(0);
}