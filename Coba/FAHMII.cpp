#include <iostream>
#include <string>

using namespace std;

int main()
{

    cout << "!!--- deskripsi awal ---!!" << endl;

    cout << "Value tipe data boolean : " << sizeof(bool) << " byte" << endl;
    cout << "Value tipe data Wchar_t : " << sizeof(wchar_t) << " byte" << endl;
    cout << "Value tipe char : " << sizeof(char) << " byte" << endl;
    cout << "Value tipe data int : " << sizeof(int) << " bytes" << endl;
    cout << "Value tipe data float : " << sizeof(float) << " bytes" << endl;
    cout << "Value tipe data double : " << sizeof(double) << " bytes" << endl;
    cout << "Value tipe data string : " << sizeof(string) << " bytes" << endl

         << endl;

    string y = "";

    cout << "Masukkan Token : ";
    cin >> y;

    bool p = atoi(y.c_str());
    int z = atoi(y.c_str());
    float x;
    double d;
    char *s = &y[0];
    int n = 0;

    if (p == false)
    {
        cout << "False ";
    }

    if (p == true)
    {
        cout << "True ";
        x = stof(y);

        d = stod(y);
    }

    cout << "string = " << y << endl
         << "boolean = " << p << endl
         << "int = " << z << endl
         << "float = " << x << endl
         << "double = " << d << endl
         << "char = " << *s << endl;

    if (z >= -2147483648 and z <= 2147483647 and z != x)
    {
        cout << "FLOAT ";

        n = 4;
    }

    else if (x <= -0.00000000000000000000000000000000000034 and x >= -34000000000000000000000000000000000000 or x >= 0.00000000000000000000000000000000000034 and x <= 34000000000000000000000000000000000000)
    {
        cout << "INT ";
        n = 4;
    }

    else if (d < -34000000000000000000000000000000000000 or d > 34000000000000000000000000000000000000)
    {
        cout << "DOUBLE ";

        n = 8;
    }

    else
    {
        cout << "CHAR ";
        n = 1;
    }

    cout << endl;
    cout << "-> dengan Ukuran data " << n << " bytes" << endl;
    return 0;
}